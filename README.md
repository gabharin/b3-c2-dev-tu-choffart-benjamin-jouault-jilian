# b3-c2-dev-tu-choffart-benjamin-jouault-jilian

## Projet à rendre

### Développer une calculatrice scientifique ayant les fonctionnalités suivantes:

- Opérations arithmétique (BDD) de base (addition, soustraction
  multiplication, division, calcule d’un pourcentage)
- Partie scientifique (TDD)
- [ ] Calcul du carré d’un nombre et de la racine carré
- [ ] Calcul de puissance d’un nombre

### Ecrire des TU

- Ecrire une documentation d’utilisation
- Ecrire le plan de test pour les deux méthodes
- Nom du repository: b3-cx-dev-tu-nom-prenom (pas demajuscule, d’espaces ni d’accent dans le nom du repo)
- Renseigner les groupes et le nom du repos sur le googlesheet
- Rendu le A définir.

### Tout manquement à une de ces règles entraînera un malus.
